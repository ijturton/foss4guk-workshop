cd styles

for /D %%i in (*) DO cd %%i & bash ..\..\scripts\load_styles & cd ..

cd ..

cd data

for /D %%i in (*) do cd %%i & bash ..\..\scripts\load_data & bash ..\..\scripts\load_rasters & cd ..

cd ..

curl -s -u admin:geoserver -X POST -H "Content-type: text/xml" http://localhost:8080/geoserver/rest/layergroups -d @fullstack/layergrp.xml

