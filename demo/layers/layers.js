var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var lyr_FullStack = new ol.layer.Tile({
                        source: new ol.source.TileWMS(({
                          url: "http://localhost:8080/geoserver/ows?version%3D1.1.1%26",
                          params: {"LAYERS": "FullStack", "TILED": "true"},
                        })),
                        title: "FullStack",
                        'type': 'base',
                        
                      });

lyr_FullStack.setVisible(true);
/*baseLayer,*//* if you add an OSM baselayer then you can see it through the VecMap & Meridian "holes" */
var layersList = [lyr_FullStack];
