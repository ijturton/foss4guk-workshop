This project is the data, styles, scripts and documentation for a workshop on
getting started with [GeoServer](http://geoserver.org)
and [Ordnance Survey](https://www.ordnancesurvey.co.uk/)'s 
[open data](https://www.ordnancesurvey.co.uk/opendatadownload/products.html).

It provides modified versions of:

+ MiniScale Raster
+ Strategi
+ Meridian
+ Vector Map District
+ OS Master Map sample

and SLD Style sheets for each layer.

If all you want is a quick install of the Open Data stack over Exeter then on a 
machine with geoserver running on `http://localhost:8080/geoserver` in the main 
directory run `scripts/do_it`. 

If you are interested in the process then follow the workflow in `workshop.pdf`.

All data remains under the [Open Government Licence](https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/).
The text of the workshop documents is available as CC BY SA NC. The scripts are GPL v2.