*****************************************Meridian™ 2**********************************************

README notes. 25/03/2013.


These styled layer descriptors (SLD files) reflect the Ordnance Survey stylesheets for January 2013
release of Meridian™ 2.

Please note that although we recommend viewing the product between 1:25,000 and 1:75,000 scale, the
minimum and maximum scale denominators in the SLDs have used the values from our OS OnDemand service.
These reflect the difference in resolution between GeoServer and many GIS'and consider the other
products available within that web service.

In these SLDs you will therefore find a minimum scale of 1:23,607 and a maximum scale of 1:94,482. These can
be removed if you wish to make the product visible outside of that scale range.

An SLD is supplied for each layer of the vector product.

To reflect our contextual style, some features may be commented out in the SLDs to prevent them from
drawing. Users can uncomment out the code to make these features appear.

A folder of SVG symbols is also included called 'meridian2symbols'. If using GeoServer please copy this
into \\data_dir\styles. We found that on recent installations with newer versions of Java the symbols
need instead to be copied into \\data_dir\workspaces\'your workspace name'\styles.

Fonts have all been set to Arial to maximise compatability.

