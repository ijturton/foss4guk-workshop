*****************************************StrategiŽ**********************************************

README notes. 25/03/2013.


These styled layer descriptors (SLD files) reflect the Ordnance Survey stylesheets for January 2013
release of Strategi.

Please note that although we recommend viewing the product between 1:125,000 and 1:175,000 scale, the
minimum and maximum scale denominators in the SLDs have used the values from our OS OnDemand service.
These reflect the difference in resolution between GeoServer and many GIS'and consider the other
products available within that web service.

In these SLDs you will therefore find a minimum scale of 1:70,860 and a maximum scale of 1:236,250. These can
be removed if you wish to make the product visible outside of that scale range.

An SLD is supplied for each layer of the vector product.

To reflect our contextual style, some features may be commented out in the SLDs to prevent them from
drawing. Users can uncomment out the code to make these features appear.

A folder of SVG symbols is included called 'strategisymbols'.
If using GeoServer please copy these into \\data_dir\styles. We found that on recent installations
with newer versions of Java the symbols need instead to be copied into \\data_dir\workspaces\'your
workspace name'\styles.

A folder 'strategifont' is also included which contains the true type font for use with the SLDs.
This font needs to be copied into your relevant font directory.
Within Windows this would be the fonts folder found through your control panel. 

Text fonts have all been set to Arial to maximise compatability.

